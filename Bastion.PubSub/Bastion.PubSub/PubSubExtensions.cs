﻿namespace Bastion.PubSub
{
    using System;

    public static class PubSubExtensions
    {

        private static readonly HubQueue globalHub = new HubQueue("Main");
//        private static readonly HubQueue hub = new HubQueue("Main");


        public static bool Exists<T>(this object obj)
        {
            foreach (var h in globalHub.handlers)
            {
                if (Equals(h.Sender.Target, obj) &&
                    typeof(T) == h.Type)
                {
                    return true;
                }
            }

            return false;
        }

        public static void Publish<T>(this T obj)
        {
            globalHub.Publish(obj);
        }

        public static void Publish<T>(this object obj, T data)
        {
            globalHub.Publish(data);
        }

        public static void Subscribe<T>(this object obj, Action<T> handler)
        {
            globalHub.Subscribe(obj, handler);
        }

        public static void Unsubscribe(this object obj)
        {
            globalHub.Unsubscribe(obj);
        }

        public static void Unsubscribe<T>(this object obj)
        {
            globalHub.Unsubscribe(obj, (Action<T>)null);
        }

        public static void Unsubscribe<T>(this object obj, Action<T> handler)
        {
            globalHub.Unsubscribe(obj, handler);
        }
    }
}
