﻿namespace Bastion.PubSub
{
    using System;
    using System.Collections.Concurrent;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;

    public class Hub
    {
        public string Name { get; set; }
        public Hub(string name)
        {
            Name = name;
        }

        internal protected class Handler
        {
            public Delegate Action { get; set; }
            public WeakReference Sender { get; set; }
            public Type Type { get; set; }
        }

        internal object locker = new object();
        internal List<Handler> handlers = new List<Handler>();



        // сюда надо добавить кеш по типу T
        // "если никто не подписался и не отписался от типа T с момента последнего вызова --
        //   просто возвращать уже закешированный список подписчиков"
        //  иначе -- пересчитать список, потом занести его в кеш и вернуть из функции
        //
        // так же в будущем можно сделать приоритеты подписчиков. Мол этот вызывается первым ,а этот - последним
        //
        protected List<Handler> getHandlers<T>()
        {
            var handlerList = new List<Handler>(handlers.Count);
            var handlersToRemoveList = new List<Handler>(handlers.Count);

            lock (this.locker)
            {
                foreach (var handler in handlers)
                {
                    if (!handler.Sender.IsAlive)
                    {
                        handlersToRemoveList.Add(handler);
                    }
                    else if (handler.Type.GetTypeInfo().IsAssignableFrom(typeof(T).GetTypeInfo()))
                    {
                        handlerList.Add(handler);
                    }
                }

                foreach (var l in handlersToRemoveList)
                {
                    handlers.Remove(l);
                }
            }

            return handlerList;
        }

        public void Publish<T>(T data = default(T))
        {
            //var handlerList = new List<Handler>(handlers.Count);
            //var handlersToRemoveList = new List<Handler>(handlers.Count);

            //lock (this.locker)
            //{
            //    foreach (var handler in handlers)
            //    {
            //        if (!handler.Sender.IsAlive)
            //        {
            //            handlersToRemoveList.Add(handler);
            //        }
            //        else if (handler.Type.GetTypeInfo().IsAssignableFrom(typeof(T).GetTypeInfo()))
            //        {
            //            handlerList.Add(handler);
            //        }
            //    }

            //    foreach (var l in handlersToRemoveList)
            //    {
            //        handlers.Remove(l);
            //    }
            //}
            List<Handler> handlerList = getHandlers<T>();

            foreach (var l in handlerList)
            {
                //Task.Run(() => { ((Action<T>) l.Action)(data); });
                // task will be done in future
                ((Action<T>)l.Action)(data);
            }
        }

        /// <summary>
        /// Allow subscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        public void Subscribe<T>(Action<T> handler)
        {
            Subscribe(this, handler);
        }

        public void Subscribe<T>(object sender, Action<T> handler)
        {
            var item = new Handler
            {
                Action = handler,
                Sender = new WeakReference(sender),
                Type = typeof(T)
            };

            lock (this.locker)
            {
                this.handlers.Add(item);
            }
        }

        /// <summary>
        /// Allow unsubscribing directly to this Hub.
        /// </summary>
        public void Unsubscribe()
        {
            Unsubscribe(this);
        }

        public void Unsubscribe(object sender)
        {
            lock (this.locker)
            {
                var query = this.handlers.Where(a => !a.Sender.IsAlive ||
                                                     a.Sender.Target.Equals(sender));

                foreach (var h in query.ToList())
                {
                    this.handlers.Remove(h);
                }
            }
        }

        /// <summary>
        /// Allow unsubscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        public void Unsubscribe<T>()
        {
            Unsubscribe<T>(this);
        }

        // сюда надо добавить кеш по типу T
        // "если никто не подписался и не отписался от типа T с момента последнего вызова --
        //   просто возвращать уже закешированный список подписчиков"
        //  иначе -- пересчитать список, потом занести его в кеш и вернуть из функции
        //
        // так же в будущем можно сделать приоритеты подписчиков. Мол этот вызывается первым ,а этот - последним
        //
        //internal List<Handler> getHandlers<T>()
        //{
        //    var handlerList = new List<Handler>(handlers.Count);
        //    var handlersToRemoveList = new List<Handler>(handlers.Count);

        //    lock (this.locker)
        //    {
        //        foreach (var handler in handlers)
        //        {
        //            if (!handler.Sender.IsAlive)
        //            {
        //                handlersToRemoveList.Add(handler);
        //            }
        //            else if (handler.Type.GetTypeInfo().IsAssignableFrom(typeof(T).GetTypeInfo()))
        //            {
        //                handlerList.Add(handler);
        //            }
        //        }

        //        foreach (var l in handlersToRemoveList)
        //        {
        //            handlers.Remove(l);
        //        }
        //    }

        //    return handlerList;
        //}

        /// <summary>
        /// Allow unsubscribing directly to this Hub.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="handler"></param>
        public void Unsubscribe<T>(Action<T> handler = null)
        {
            Unsubscribe<T>(this, handler);
        }

        public void Unsubscribe<T>(object sender, Action<T> handler = null)
        {
            lock (this.locker)
            {
                var query = this.handlers
                    .Where(a => !a.Sender.IsAlive ||
                                (a.Sender.Target.Equals(sender) && a.Type == typeof(T)));

                if (handler != null)
                {
                    query = query.Where(a => a.Action.Equals(handler));
                }

                foreach (var h in query.ToList())
                {
                    this.handlers.Remove(h);
                }
            }
        }

        public override string ToString()
        {
            return string.Format($"Hub: {Name}");
        }
    }



    // эксперимент с постановкой объектов публикуемых -- в очередь
    public class HubQueue : Hub
    {
        Thread thread;
        public HubQueue(string name) : base(name)
        {
            thread = new Thread(ProcessEvents)
            {
                Name = "HubQueue thread " + name
            };
            thread.Start();
        }
        //Queue<List<Handler>> queue = new Queue<List<Handler>>()
        class Msg
        {
            public List<Handler> Handlers { get; set; }
            public object Data { get; set; }

            public void Process()
            {
                foreach (var l in Handlers)
                    l.Action.DynamicInvoke(Data);
            }
        }
        //ConcurrentQueue<List<Handler>> queue = new ConcurrentQueue<List<Handler>>();
        //ConcurrentQueue<object> queue = new ConcurrentQueue<object>();
        //ConcurrentQueue<Msg> queue = new ConcurrentQueue<Msg>();
        BlockingCollection<Msg> queue = new BlockingCollection<Msg>();

        public new void Publish<T>(T data = default(T))
        {
            var msg = new Msg()
            {
                Handlers = getHandlers<T>(),
                Data = data
            };
            //queue.Enqueue(msg);
            Debug.WriteLine($"> publish: {data}");
            queue.Add(msg);

          //  queue.CompleteAdding();
            //  queue.CompleteAdding();
        }

        void ProcessEvents(object o)
        {
            while (true)
            {
                Debug.WriteLine(".. start thread");
                foreach (var msg in queue.GetConsumingEnumerable())
                {
                    foreach (var l in msg.Handlers)
                        l.Action.DynamicInvoke(msg.Data);
                    Debug.WriteLine("");
                }

                Debug.WriteLine(".. end thread");
            }
        }


    }
}
